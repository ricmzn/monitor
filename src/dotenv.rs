use std::io::{BufRead, BufReader};
use std::result::Result;
use std::env::set_var;
use std::fs::File;

type ParseResult<T> = Result<T, String>;

fn parse(line: &str) -> ParseResult<Option<(&str, &str)>> {
    let line = line.trim();
    if line.is_empty() || line.starts_with("#") {
        Ok(None)
    } else {
        let mut parts = line.splitn(2, "=");
        let name = parts.next().ok_or("Parse error")?.trim();
        let value = parts.next().ok_or_else(|| format!("Missing equals sign in .env at line: {}", line))?.trim();
        Ok(Some((name, value)))
    }
}

pub fn dotenv() -> ParseResult<()> {
    let file = File::open(".env");
    if let Ok(file) = file {
        let reader = BufReader::new(file);
        for line in reader.lines() {
            let line = line.or(Err("Error reading line from .env file"))?;
            if let Some((name, value)) = parse(&line)? {
                set_var(name, value);
            }
        }
    }
    Ok(())
}
