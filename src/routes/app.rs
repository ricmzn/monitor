use actix_web::HttpResponse;
use http::StatusCode;
use serde::*;

#[derive(Serialize)]
pub struct Message {
    message: String,
    subtitle: &'static str
}

pub fn index() -> HttpResponse {
    HttpResponse::Ok().json(Message {
        message: "Hello, World!".into(),
        subtitle: "Deployed with Docker".into()
    })
}

pub fn healthcheck() -> HttpResponse {
    HttpResponse::Ok().finish()
}

pub fn not_found() -> HttpResponse {
    HttpResponse::build(StatusCode::NOT_FOUND).finish()
}
