use actix_web::{HttpRequest, HttpResponse, HttpMessage, dev::ConnectionInfo};
use actix_web::{Error as ActixError, error::ErrorInternalServerError};
use actix_web::cookie::{Cookie, SameSite};

use std::error::Error;
use std::env::var;

use serde::{Serialize, Deserialize};
use serde_json::to_string;
use oauth2::Config;

#[derive(Serialize, Deserialize)]
pub struct GoogleUserInfo {
    id: String,
    email: String,
    verified_email: bool,
    name: String,
    given_name: String,
    family_name: String,
    picture: String,
    locale: String
}

fn oauth(info: &ConnectionInfo) -> Config {
    Config::new(
        var("GOOGLE_CLIENT_ID").expect("Missing Google client ID"),
        var("GOOGLE_CLIENT_SECRET").expect("Missing Google client secret"),
        "https://accounts.google.com/o/oauth2/v2/auth",
        "https://oauth2.googleapis.com/token"
    )
    .add_scope("profile")
    .add_scope("email")
    .set_redirect_url(format!("{}://{}/api/login/google", info.scheme(), info.host()))
}

fn exchange_code(info: &ConnectionInfo, code: &str) -> Result<String, Box<Error>> {
    let token = oauth(info)
        .exchange_code(code)?
        .access_token;
    Ok(token)
}

fn get_user_info(access_token: &str) -> Result<GoogleUserInfo, Box<dyn Error>> {
    let mut response: GoogleUserInfo = reqwest::Client::new()
        .get("https://www.googleapis.com/oauth2/v2/userinfo")
        .bearer_auth(access_token)
        .send()?
        .json()?;
    // Change picture to smaller version to avoid being rate-limited by Google
    response.picture = response.picture.replace("/mo/", "/s32-mo/");
    Ok(response)
}

pub fn google_login(req: HttpRequest) -> Result<HttpResponse, ActixError> {
    let info = req.connection_info();
    match req.cookie("code") {
        Some(code) => {
            let access_token = exchange_code(&info, code.value()).map_err(|e| ErrorInternalServerError(e))?;
            let user_info = get_user_info(&access_token).map_err(|e| ErrorInternalServerError(e))?;
            let user_json = to_string(&user_info).map_err(|e| ErrorInternalServerError(e))?;
            let user_cookie = Cookie::build("user", user_json)
                .same_site(SameSite::Lax)
                .path("/")
                .finish();
            Ok(
                HttpResponse::Ok()
                    .cookie(user_cookie)
                    .set_header("Location", format!("{}://{}", info.scheme(), info.host()))
                    .finish()
            )
        },
        None => Ok(
            HttpResponse::Ok()
                .set_header("Location", oauth(&info).authorize_url().to_string())
                .finish()
        )
    }
}
