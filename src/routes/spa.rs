use actix_web::HttpRequest;
use actix_files::NamedFile;
use std::path::Path;

const STATIC_FILES: &[&str] = &[
    "index.html",
    "robots.txt",
    "manifest.json",
    "service-worker.js",
    "favicon.ico",
];

pub fn serve(req: HttpRequest) -> Option<NamedFile> {
    // Get the relative request path
    let path = Path::new(req.path()).strip_prefix("/").ok()?;
    // Get a string of the path to compare with STATIC_FILES
    let path_str = path.to_str().unwrap_or("");
    // Check if the path is a standard static file
    if path.starts_with("assets/")
        || path.starts_with("img/")
        || STATIC_FILES.contains(&path_str)
    {
        // Serve it from the static files dir
        let relative_path = Path::new("public").join(path);
        NamedFile::open(relative_path).ok()
    } else {
        // Otherwise, serve the SPA itself
        NamedFile::open("public/index.html").ok()
    }
}
