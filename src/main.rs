use actix_web::{web::get, App, HttpServer};

mod routes;
mod dotenv;

fn main() {
    color_backtrace::install();
    dotenv::dotenv().unwrap();
    let server = HttpServer::new(|| {
        App::new()
            .route("/api/",             get().to(routes::app::index))
            .route("/api/healthcheck",  get().to(routes::app::healthcheck))
            .route("/api/login/google", get().to(routes::auth::google_login))
            .route("/api/{path:.*}",    get().to(routes::app::not_found))
            .route("{path:.*}",         get().to(routes::spa::serve))
    });
    server.bind("0.0.0.0:8080")
        .map(|server| { eprintln!("Server listening on http://localhost:8080/"); server })
        .unwrap()
        .run()
        .unwrap();
}
