FROM ubuntu:disco

RUN apt-get update -q
RUN apt-get install -yq libssl1.1 curl

WORKDIR /app
ENV PATH=$PATH:/app/bin

COPY public/ public/
COPY bin/ bin/

EXPOSE 8080
CMD [ "monitor" ]

HEALTHCHECK CMD curl --fail --silent http://localhost:8080/api/healthcheck || exit 1
