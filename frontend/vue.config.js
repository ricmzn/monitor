const RemoveServiceWorkerPlugin = require("webpack-remove-serviceworker-plugin");

process.env.BASE_URL = process.env.BASE_URL || "/";

/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
  lintOnSave: false,
  outputDir: "../public",
  assetsDir: "assets",
  devServer: {
    port: 3000,
    proxy: {
      "^/api": {
        target: "http://localhost:8080",
        headers: {
          Host: "localhost:3000"
        }
      }
    }
  },
  configureWebpack: {
    plugins: [
      new RemoveServiceWorkerPlugin({ filename: "service-worker.js" })
    ]
  }
};
