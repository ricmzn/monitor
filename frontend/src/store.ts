import { Module, VuexModule, Mutation, Action, getModule } from "vuex-module-decorators";
import Vue from "vue";
import Vuex from "vuex";
import api from "@/api";

Vue.use(Vuex);

interface User {
    fullName: string;
    shortName: string;
    picture: string;
    email: string;
}

@Module({ name: "app" })
export class App extends VuexModule {

    public darkMode = false;
    public token?: string;
    public user?: User;

    @Mutation
    public setDarkMode(enabled: boolean) {
        this.darkMode = enabled;
    }

    @Mutation
    public setToken(token?: string) {
        this.token = token;
    }

    @Mutation
    public setUser(user?: User) {
        this.user = user;
    }

    @Action
    public async login() {
        const response = await api.get("/login/google");
    }

    @Action
    public async logout() {
        this.setToken(undefined);
        this.setUser(undefined);
    }

}

const store = new Vuex.Store({
    modules: {
        app: App
    }
});

export default store;

export const appState = getModule(App, store);
