import Vue from "vue";
import Vuetify from "vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify, {
    iconfont: "mdi",
});

const vm = new Vue({
    router,
    store,
    el: "#app",
    render: (h) => h(App)
});
