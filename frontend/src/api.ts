import { appState } from "@/store";
import axios from "axios";

const api = axios.create({
    baseURL: "/api"
});

api.interceptors.request.use((config) => {
    if (appState.token) {
        config.headers.Authorization = `Bearer ${appState.token}`;
    }
    return config;
});

export default api;
