#!/bin/bash -e

OPTS=$(getopt --options '-h' --longoptions all,frontend,api,help -- "$@")
BUILD_FRONTEND=
BUILD_API=

case "$OPTS" in
    *'--all'*)
        BUILD_FRONTEND=1
        BUILD_API=1
    ;;
    *'--frontend'*)
        BUILD_FRONTEND=1
    ;;
    *'--api'*)
        BUILD_API=1
    ;;
    *)
        echo ""
        echo "Usage: $0 <targets>"
        echo ""
        echo "Targets:"
        echo "  --all        Build API and front-end"
        echo "  --frontend   Build only the Vue.js front-end"
        echo "  --api        Build only the Rust API"
        echo ""
    ;;
esac

# Front-end
if [ "$BUILD_FRONTEND" ]; then
(
    cd frontend
    npm ci
    npm run build
)
fi

# API
if [ "$BUILD_API" ]; then
(
    rm -rf bin
    cargo build --release
    mkdir -p bin
    cp target/release/monitor bin/monitor
)
fi
