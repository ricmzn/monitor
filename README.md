## Development Setup

* Install Node.js from https://nodejs.org

* Install Rust (nightly) from https://rustup.rs

* Install cargo-watch: `cargo install cargo-watch`

* Open project in Visual Studio Code

* Start with Ctrl+Shift+B

* Open browser at http://localhost:3000

## Pre-Built Binaries

Currently, the only pre-built option is available via Docker:  
`docker pull registry.gitlab.com/kukiric/monitor:latest`
